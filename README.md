Primeiramente o projeto foi criado a partir do [ignite](https://github.com/infinitered/ignite), que gera o projeto com algumas dependências já instaladas, algumas delas são:

[apisauce](https://github.com/infinitered/apisauce): torna o consumo de APIs mais simples, como pode ser visto no arquivo Api.js

[react-native-config](https://github.com/luggit/react-native-config): Não foi utilizada de fato no projeto, mas geralmente utilizo para configuração de variáveis de ambiente (development, staging, production)

[react-native-device-info](https://github.com/react-native-community/react-native-device-info): Também não foi utilizada no projeto, mas geralmente utilizo para obter informações dos dispositivos que estão rodando o app, é uma lib bem completa com muitas informações relevantes.

[react-native-gesture-handler](https://github.com/software-mansion/react-native-gesture-handler): Não foi utilizada por mim no projeto, porém geralmente a utilizo para detectar toques na tela, e utilizar juntamente com alguma biblioteca de animação.

[react-navigation](https://reactnavigation.org/): Gerenciar a navegação do app,

[react-redux](https://react-redux.js.org/): Gerenciamento do estado global da aplicação

[redux-saga](https://redux-saga.js.org/):  Utilizada para lidar com efeitos colaterais da aplicação

[reduxsauce](https://github.com/jkeam/reduxsauce): Utilizada para simplificar e padronizar a escritas dos arquivo de Redux, (i.e. reducers, tipos, ações)

Algumas das dependências não foram removidas, também por falta de tempo.

Executar 
===
iOS

1. Clonar o repositório
     - Executar no terminal o comando git clone link
2. Navegar até a pasta ioasys
    - Executar o comando cd ioasys
3. Instalar as dependências do projeto
    - Executar o comando yarn && cd ios && pod install
4. Voltar para a pasta ioasys e rodar o projeto
    - Executar o comando cd .. && react-native run-ios

Android

Repetir os passos 1 e 2 acima

3. Instalar as dependências do projeto
    - Executar o comando yarn
4. Abrir o Android Studio na pasta android do projeto
    - Abrir o diretorio ˜/ioasys/android no Android Studio
5. Sincronizar o projeto
    - Sincronizar apertando o botao na direita em cima com uma setinha azul
6. Com um dispositivo (ou emulador) conectado, executar o projeto
    - Executar o comando react-native run-android na pasta ioasys


No projeto foi implementada a chamada dos 4 endpoints disponibilizados
- Autenticação
- Enterprises
- EnterprisesById
- FilteredEnterprises

Também foram criadas as telas de listar empresas e detalhes da empresa.

O que não deu tempo de fazer:
- Criar tela de login utilizando formulários
- Cenários de erro para as chamadas de endpoint
- Criar o filtro na tela de listar empresas (ficou hard coded)
- Utilizar as informações do usuário que são retornadas ao autenticá-lo
- Utilizar mais campos do retorno de detalhes da empresa
- Animações
- Testes de Seletores
- Utilizar storybook para os componentes
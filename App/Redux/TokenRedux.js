import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  tokenRequest: ['email', 'password'],
  tokenSuccess: ['data'],
  tokenFailure: ['error'],
  getEnterprises: null
})

export const TokenTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: {},
  fetching: null,
  error: null
})

/* ------------- Reducers ------------- */

export const tokenRequest = (state) =>
  state.merge({ fetching: true, error: false })

export const enterpriseRequest = (state) =>
  state.merge({ fetching: true, error: false })

export const tokenSuccess = (state, { data }) =>
  state.merge({ fetching: false, error: null, data })

export const tokenFailure = (state) =>
  state.merge({ fetching: false, error: true })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TOKEN_REQUEST]: tokenRequest,
  [Types.TOKEN_SUCCESS]: tokenSuccess,
  [Types.TOKEN_FAILURE]: tokenFailure,
  [Types.GET_ENTERPRISES]: enterpriseRequest
})

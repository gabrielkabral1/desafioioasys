import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  enterprisesRequest: ['accessToken', 'clientToken', 'uId'],
  enterprisesRequestEnterpriseById: ['accessToken', 'clientToken', 'uId', 'enterpriseId'],
  enterprisesRequestFilteredEnterprises: ['accessToken', 'clientToken', 'uId', 'enterpriseType', 'enterpriseName'],
  enterprisesSelectEnterprise: ['selectedEnterprise'],
  enterprisesSuccess: ['enterprises'],
  enterprisesFailure: ['error']
})

export const EnterprisesTypes = Types
export default Creators

export type Enterprise = {
  id: number,
  enterprise_name: string,
  description: string,
  email_enterprise: ?string,
  facebook: ?string,
  twitter: ?string,
  linkedin: ?string,
  phone: ?string,
  own_enterprise: boolean,
  photo: ?string,
  value: number,
  shares: number,
  share_price: number,
  own_shares: number,
  city: string,
  country: string
}

export type Enterprises = Enterprise[]

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  enterprises: {},
  selectedEnterprise: {},
  fetching: null,
  error: null
})

/* ------------- Reducers ------------- */

export const enterprisesRequest = (state) =>
  state.merge({ fetching: true, error: false })

export const selectEnterpriseById = (state, { selectedEnterprise }) =>
  state.merge({ fetching: false, error: false, selectedEnterprise })

export const enterprisesSucccess = (state, { enterprises }) =>
  state.merge({ fetching: false, error: null, enterprises })

export const enterprisesFailure = (state) =>
  state.merge({ fetching: false, error: true, enterprises: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ENTERPRISES_REQUEST]: enterprisesRequest,
  [Types.ENTERPRISES_REQUEST_ENTERPRISE_BY_ID]: enterprisesRequest,
  [Types.ENTERPRISES_REQUEST_FILTERED_ENTERPRISES]: enterprisesRequest,
  [Types.ENTERPRISES_SELECT_ENTERPRISE]: selectEnterpriseById,
  [Types.ENTERPRISES_SUCCESS]: enterprisesSucccess,
  [Types.ENTERPRISES_FAILURE]: enterprisesFailure
})

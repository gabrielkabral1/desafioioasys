export const fetching = (state) => state && state.enterprises.fetching
export const error = (state) => state.enterprises.error
export const enterprises = (state) => state.enterprises.enterprises
export const selectedEnterprise = (state) => state.enterprises.selectedEnterprise

export default {
  fetching,
  error,
  enterprises,
  selectedEnterprise
}

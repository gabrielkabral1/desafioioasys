export const accessToken = (state) => state && state.token.data.accessToken
export const clientToken = (state) => state.token.data.clientToken
export const uId = (state) => state.token.data.uId

export default {
  accessToken,
  clientToken,
  uId
}

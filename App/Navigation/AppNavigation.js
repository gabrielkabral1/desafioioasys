import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import LaunchScreen from '../Containers/LaunchScreen'
import EnterprisesScreen from '../Containers/EnterprisesScreen'
import EnterpriseDetailsScreen from '../Containers/EnterpriseDetailsScreen'

import styles from './Navigation.styles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  LaunchScreen,
  EnterprisesScreen,
  EnterpriseDetailsScreen
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)

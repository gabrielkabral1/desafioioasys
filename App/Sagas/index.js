import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'

import { TokenTypes } from '../Redux/TokenRedux'
import { EnterprisesTypes } from '../Redux/EnterprisesRedux'

/* ------------- Sagas ------------- */
import { autenticationToken } from './TokenSagas'
import { getEnterprises, getEnterprisesById, getFilteredEnterprises } from './EnterprisesSagas'
/* ------------- API ------------- */

const api = API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    takeLatest(TokenTypes.TOKEN_REQUEST, autenticationToken, api),
    takeLatest(EnterprisesTypes.ENTERPRISES_REQUEST, getEnterprises, api),
    takeLatest(EnterprisesTypes.ENTERPRISES_REQUEST_ENTERPRISE_BY_ID, getEnterprisesById, api),
    takeLatest(EnterprisesTypes.ENTERPRISES_REQUEST_FILTERED_ENTERPRISES, getFilteredEnterprises, api)
  ])
}

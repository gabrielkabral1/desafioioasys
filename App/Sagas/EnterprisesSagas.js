import { call, put } from 'redux-saga/effects'
import TokenActions from '../Redux/TokenRedux'
import EnterprisesActions from '../Redux/EnterprisesRedux'

export function * getEnterprises (api, { accessToken, clientToken, uId }) {
  const response = yield call(api.getEnterprises, { accessToken, clientToken, uId })
  if (response.ok) {
    const { data: { enterprises } } = response
    yield put(EnterprisesActions.enterprisesSuccess(enterprises))
  } else {
    yield put(TokenActions.tokenFailure({ error: 'Não foi possível obter as empresas.' }))
  }
}

export function * getEnterprisesById (api, { accessToken, clientToken, uId, enterpriseId }) {
  const response = yield call(api.getEnterprisesById, { accessToken, clientToken, uId, enterpriseId })
  if (response.ok) {
    const { data: { enterprise } } = response
    yield put(EnterprisesActions.enterprisesSelectEnterprise(enterprise))
  } else {
    yield put(EnterprisesActions.enterprisesFailure({ error: 'Não foi possível obter a empresa selecionada.' }))
  }
}

export function * getFilteredEnterprises (api, { accessToken, clientToken, uId, enterpriseType, enterpriseName }) {
  const response = yield call(api.getFilteredEnterprises, { accessToken, clientToken, uId, enterpriseType, enterpriseName })
  if (response.ok) {
    const { data } = response
    // yield put(EnterprisesActions.enterprisesSelectEnterprise(data))
  } else {
    yield put(EnterprisesActions.enterprisesFailure({ error: 'Não foi possível obter a empresa selecionada.' }))
  }
}

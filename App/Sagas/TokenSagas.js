import { call, put } from 'redux-saga/effects'
import TokenActions from '../Redux/TokenRedux'
import EnterprisesActions from '../Redux/EnterprisesRedux'
import { NavigationActions } from 'react-navigation'

export function * autenticationToken (api, action) {
  const { email, password } = action
  const response = yield call(api.getToken, { email, password })
  if (response.ok) {
    const { headers } = response
    const data = {
      accessToken: headers['access-token'],
      clientToken: headers.client,
      uId: headers.uid
    }
    yield put(TokenActions.tokenSuccess(data))
    yield put(EnterprisesActions.enterprisesRequest(data.accessToken, data.clientToken, data.uId))
    yield put(NavigationActions.navigate({ routeName: 'EnterprisesScreen' }))
  } else {
    yield put(TokenActions.tokenFailure({ error: 'Não foi possível autenticar o usuário.' }))
  }
}

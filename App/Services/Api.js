import apisauce from 'apisauce'

const create = (baseURL = 'https://empresas.ioasys.com.br/') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json'
    },
  })

  const getToken = () => api.post('api/v1/users/auth/sign_in', { email: 'testeapple@ioasys.com.br', password: '12341234' })
  const getEnterprises = ({ accessToken, clientToken, uId }) => api.get('api/v1/enterprises', {}, { headers: { 'access-token': accessToken, client: clientToken, uid: uId } })
  const getEnterprisesById = ({ accessToken, clientToken, uId, enterpriseId }) => api.get(`api/v1/enterprises/${enterpriseId}`, {}, { headers: { 'access-token': accessToken, client: clientToken, uid: uId } })
  const getFilteredEnterprises = ({ accessToken, clientToken, uId, enterpriseType, enterpriseName }) => api.get(`api/v1/enterprises?enterprise_types=${enterpriseType}`, {}, { headers: { 'access-token': accessToken, client: clientToken, uid: uId } })
  const getRate = () => api.get('rate_limit')
  const getUser = (username) => api.get('search/users', {q: username})

  return {
    getToken,
    getEnterprises,
    getEnterprisesById,
    getFilteredEnterprises,
    getRate,
    getUser
  }
}

export default {
  create
}

// @flow
import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import type { Enterprise } from '../Redux/EnterprisesRedux'
import styles from './EnterpriseCard.style'

type Props = {
  enterprise: Enterprise,
  onPressEnterpriseCard: () => mixed
}

const EnterpriseCard = ({ enterprise, onPressEnterpriseCard }: Props) => {
  const fixedEnterpriseImage = 'https://404store.com/2018/08/16/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.md.jpg'
  return (
    <TouchableOpacity onPress={onPressEnterpriseCard} style={styles.mainContainer}>
      <Image source={{ uri: fixedEnterpriseImage }} style={styles.image} />
      <View style={styles.textsContainer}>
        <Text style={styles.title}>{enterprise.enterprise_name}</Text>
        <Text style={styles.subtitle}>{enterprise.enterprise_type.enterprise_type_name.toUpperCase()}</Text>
        <Text ellipsizeMode={'tail'} numberOfLines={3} style={styles.description}>{enterprise.description}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default EnterpriseCard

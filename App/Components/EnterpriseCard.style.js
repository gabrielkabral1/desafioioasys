import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    marginBottom: 10,
    marginHorizontal: 16,
    height: 230,
    borderRadius: 5,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 2
    },
    elevation: 4
  },
  image: {
    flex: 1,
    margin: 5,
    borderTopRightRadius: 3,
    borderTopLeftRadius: 3
  },
  textsContainer: {
    paddingHorizontal: 12,
    paddingBottom: 10
  },
  title: {
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: -0.36,
    color: '#3E3F40'
  },
  subtitle: {
    fontSize: 9,
    lineHeight: 16,
    fontWeight: '500',
    color: '#AFB1B3'
  },
  description: {
    fontSize: 12,
    lineHeight: 16,
    color: '#707273',
    opacity: 0.75
  }
})

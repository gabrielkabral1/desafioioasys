import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  safeArea: {
    flex: 1
  },
  headerContainer: {
    height: 80,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    flexDirection: 'row'
  },
  headerTitle: {
    color: '#3E3F40',
    fontSize: 21,
    fontWeight: 'bold',
    letterSpacing: -0.24,
    lineHeight: 26
  },
  headerAvatar: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2
  },
  scrollContent: {
    backgroundColor: '#f0f2f2',
    paddingVertical: 16
  },
  fetching: {
    flex: 1,
    alignItems: 'center'
  }
})

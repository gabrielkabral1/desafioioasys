// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableOpacity, Text, View } from 'react-native'
import TokenActions from '../Redux/TokenRedux'
import EnterprisesActions from '../Redux/EnterprisesRedux'
import TokenSelectors from '../Selectors/TokenSelectors'

// Styles
import styles from './LaunchScreenStyles'

type Props = {
  getToken: (email: string, password: string) => mixed,
}

class LaunchScreen extends Component<Props> {
  render () {
    return (
      <View style={[styles.mainContainer, { justifyContent: 'space-around', paddingHorizontal: 16 }]}>
        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, height: 200, paddingHorizontal: 16 }} onPress={() => this.props.getToken('testeapple@ioasys.com.br', '12341234')}>
          <Text>1. Para autenticar o usuário e navegar para a tela seguinte clique aqui</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  accessToken: TokenSelectors.accessToken(state),
  clientToken: TokenSelectors.clientToken(state),
  uId: TokenSelectors.uId(state)
})

const mapDispatchToProps = {
  getToken: TokenActions.tokenRequest,
  getFilteredEnterprises: EnterprisesActions.enterprisesRequestFilteredEnterprises
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)

import React from 'react'
import { View, Text, Image, ActivityIndicator, TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'
import EnterpriseSelectors from '../Selectors/EnterpriseSelectors'
import styles from './EnterpriseDetailsScreen.style'
import { Images } from '../Themes'

const EnterpriseDetailsScreen = ({ navigation }) => {
  const fetching = useSelector(EnterpriseSelectors.fetching)
  const selectedEnterprise = useSelector(EnterpriseSelectors.selectedEnterprise)

  return (
    <View style={styles.mainContainer}>
      {fetching && <ActivityIndicator size={'large'} style={styles.fetching} />}
      {!fetching &&
        <View style={styles.flex}>
          <Image style={styles.enterpriseImage} resizeMode={'cover'} source={{ uri: 'https://404store.com/2018/08/16/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.md.jpg' }} />
          <CloseButton onPressClose={() => navigation.goBack(null)} />
          <View style={styles.textsContainer}>
            <Text style={[styles.generalText, styles.title]}>{selectedEnterprise.enterprise_name}</Text>
            <Text style={[styles.generalText, styles.type]}>{selectedEnterprise.enterprise_type.enterprise_type_name}</Text>
            <Text style={[styles.generalText, styles.description]}>{selectedEnterprise.description}</Text>
            <Text style={[styles.generalText, styles.location]}>{`Location: ${selectedEnterprise.city}, ${selectedEnterprise.country}`}</Text>
            <Text style={styles.generalText}>Shares: {selectedEnterprise.shares}</Text>
            <Text style={styles.generalText}>Share Price: {selectedEnterprise.share_price}</Text>
          </View>
        </View>
      }

    </View>
  )
}

const CloseButton = ({ onPressClose }) => (
  <TouchableOpacity onPress={onPressClose} style={{ position: 'absolute', top: 50, right: 20 }}>
    <Image source={Images.closeButton} />
  </TouchableOpacity>
)
export default EnterpriseDetailsScreen

import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  flex: {
    flex: 1
  },
  fetching: {
    flex: 1,
    alignItems: 'center'
  },
  enterpriseImage: {
    flex: 1,
    maxHeight: '30%'
  },
  textsContainer: {
    paddingHorizontal: 16
  },
  generalText: {
    marginTop: 4,
    fontSize: 15,
    lineHeight: 19,
    letterSpacing: -0.59,
    color: '#3E3F40',
    fontWeight: '500'
  },
  title: {
    marginTop: 14,
    fontSize: 23,
    lineHeight: 28
  },
  type: {
    color: '#AFB1B3',
    opacity: 0.75
  },
  description: {
    color: '#707273',
    opacity: 0.7,
    marginTop: 21
  },
  location: {
    marginTop: 16
  }
})

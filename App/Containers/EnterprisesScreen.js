// @flow
import React, {useCallback} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {
  View,
  Text,
  Image,
  SafeAreaView,
  ScrollView,
  ActivityIndicator, TouchableOpacity
} from 'react-native'
import EnterpriseSelectors from '../Selectors/EnterpriseSelectors'
import EnterpriseActions from '../Redux/EnterprisesRedux'
import TokenSelectors from '../Selectors/TokenSelectors'
import EnterpriseCard from '../Components/EnterpriseCard'
import styles from './EnterprisesScreen.style'
import type Enterprises from '../Redux/EnterprisesRedux'

type Props = {
  navigation: *
}

const EnterprisesScreen = ({ navigation }: Props) => {
  // Selectors
  const accessToken: string = useSelector(TokenSelectors.accessToken)
  const clientToken: string = useSelector(TokenSelectors.clientToken)
  const uId: string = useSelector(TokenSelectors.uId)
  const enterprises: Enterprises = useSelector(EnterpriseSelectors.enterprises)
  const fetching: boolean = useSelector(EnterpriseSelectors.fetching)
  // @TODO implementar cenários de erro
  // const error: ?string = useSelector(EnterpriseSelectors.error)

  // Dispatchs
  const dispatch = useDispatch()
  const getEnterpriseById = useCallback(
    (accessToken, clientToken, uId, enterpriseId) =>
      dispatch(
        EnterpriseActions.enterprisesRequestEnterpriseById(
          accessToken,
          clientToken,
          uId,
          enterpriseId
        )
      ),
    [dispatch]
  )
  const getFilteredEnterprises = useCallback(
    (accessToken, clientToken, uId, type, name) =>
      dispatch(
        EnterpriseActions.enterprisesRequestEnterpriseById(
          accessToken,
          clientToken,
          uId,
          type,
          name
        )
      ),
    [dispatch]
  )
  // Functions
  const onPressEnterpriseCard = ({
    accessToken,
    clientToken,
    uId,
    enterpriseId
  }) => {
    getEnterpriseById(accessToken, clientToken, uId, enterpriseId)
    navigation.navigate('EnterpriseDetailsScreen')
  }

  return (
    <SafeAreaView style={styles.safeArea}>
      <View>
      <View style={styles.headerContainer}>
        <Text style={styles.headerTitle}>Olá, Gabriel!</Text>
        <Image
          style={styles.headerAvatar}
          source={{
            uri: 'https://avatars1.githubusercontent.com/u/33816395?s=460&v=4'
          }}
        />
      </View>
        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, padding: 16 }} onPress={() => getFilteredEnterprises(accessToken, clientToken, uId, '1', '')}>
          <Text>{'2. Para fazer uma chamada de filteredEnterprises clique aqui. A chama está hard coded, por falta de tempo, só será possível observá-la no console.log ou console.tron.log'}</Text>
        </TouchableOpacity>
      </View>
      {fetching && <ActivityIndicator style={styles.fetching} size={'large'} />}
      {!fetching && !!enterprises && (
        <ScrollView
          bounces={false}
          contentContainerStyle={styles.scrollContent}>
          {enterprises.map(enterprise => (
            <EnterpriseCard
              onPressEnterpriseCard={() =>
                onPressEnterpriseCard({
                  accessToken,
                  clientToken,
                  uId,
                  enterpriseId: enterprise.id
                })
              }
              key={`${enterprise.id}${enterprise.enterprise_name}`}
              enterprise={enterprise}
            />
          ))}
        </ScrollView>
      )}
    </SafeAreaView>
  )
}

export default EnterprisesScreen
